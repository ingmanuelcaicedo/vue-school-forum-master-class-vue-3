import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store'

// Components
import Home from '@/pages/Home'
import NotFound from '@/pages/NotFound'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: NotFound
  },
  /* {
    path: '/eror-404',
    name: 'NotFound',
    component: NotFound
  }, */
  {
    path: '/forum/:id',
    name: 'Forum',
    component: () => import('@/pages/Forum'),
    props: true
  },
  {
    path: '/category/:id',
    name: 'Category',
    component: () => import('@/pages/Category'),
    props: true
  },
  {
    path: '/thread/:id',
    name: 'ThreadShow',
    component: () => import('@/pages/ThreadShow'),
    props: true,
    beforeEnter (to, from, next) {
      // If the Thread Exist
      const threadExists = store.state.threads.find(thread => thread.id === to.params.id)
      // If exists continue
      if (threadExists) {
        return next()
      } else {
        next({
          name: 'NotFound',
          params: { pathMatch: to.path.substring(1).split('/') },
          // Next two values preserve existing query and hash
          query: to.query,
          hash: to.hash
        })
      }
      // If does not exits redirect to not found
    }
  },
  {
    name: 'Profile',
    path: '/me',
    component: () => import('@/pages/Profile'),
    meta: { toTop: true, smothScroll: true }
  },
  {
    name: 'ProfileEdit',
    path: '/me/edit',
    component: () => import('@/pages/Profile'),
    props: { edit: true }
  }
]

export default createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior (to) {
    const scroll = {
    }
    if (to.meta.toTop) {
      scroll.top = 0
    }
    if (to.meta.smothScroll) {
      scroll.behavior = 'smooth'
    }
    return scroll
  }
})
